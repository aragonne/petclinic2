def ip

pipeline{
    agent any
    tools {
        maven 'mvn'
        jdk 'jdk-11'
    }    
    stages {
        stage ('clone'){
            steps{
                git branch: 'main', url: 'https://gitlab.com/aragonne/petclinic2.git'
            }
        }
        stage ('Maven Build') {
            steps {
                sh 'mvn clean install -P MySQL -f $HOME/.jenkins/workspace/test_pipeline/petclinic-pipeline/pom.xml'
            }
        }
        stage ('Testing Stage') {
            steps {
                sh 'mvn test -f $HOME/.jenkins/workspace/test_pipeline/petclinic-pipeline/pom.xml'
            } 
        }
        stage('Execute Performance Tests') {
            steps {
                sh 'jmeter -n -t $HOME/.jenkins/workspace/test_pipeline/petclinic-pipeline/src/test/jmeter/petclinic_test_plan.jmx -l test.jml' 
            }
        }
        stage('Build') {
            steps{
                sh 'docker build -t testacrar.azurecr.io/app-petclinic:v0.1 -f petclinic-pipeline/Dockerfile .'
            }
        }
        stage('deploy with terraform') {
            steps {
                sh 'terraform init'
                sh 'terraform apply -auto-approve'
            }
        }
        stage('Publish') {
            steps {
                withCredentials([azureServicePrincipal('AzureServicePrincipal')]) {
                    sh 'az login --service-principal -u $AZURE_CLIENT_ID -p $AZURE_CLIENT_SECRET -t $AZURE_TENANT_ID'
                    sh 'az account set -s $AZURE_SUBSCRIPTION_ID'
                    sh "docker login -u ${AZURE_CLIENT_ID} -p ${AZURE_CLIENT_SECRET} testacrar.azurecr.io"
                    sh "docker push testacrar.azurecr.io/app-petclinic:v0.1"
                    
                }
            }
        }
        stage('AKS') {
            steps {
                withCredentials([azureServicePrincipal('AzureServicePrincipal')]) {
                    sh 'az login --service-principal -u $AZURE_CLIENT_ID -p $AZURE_CLIENT_SECRET -t $AZURE_TENANT_ID'
                    sh 'az account set -s $AZURE_SUBSCRIPTION_ID'
                    sh "az aks get-credentials --resource-group petclinic-rg --name petclinic-cluster"
                    sh 'az acr login -n testacrar'
                    sh 'kubectl apply -f deployment-petclinic.yml'
                }
            }
        }
    }
}
